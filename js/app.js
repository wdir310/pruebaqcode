// variables
const valor = document.getElementById('valor');
const listItems = document.getElementById('listItems');
const trm = document.getElementById('trm');
const btnEnviar = document.getElementById('enviar');
const formularioEnviar = document.getElementById('enviar-datos');
const resetBtn = document.getElementById('resetBtn');

// event Listener

eventListeners();

function eventListeners() {
     // Inicio de la aplicación y deshabilitar submit
     document.addEventListener('DOMContentLoaded', inicioApp);

     // Campos del formulario
     valor.addEventListener('blur', validarCampo);
     listItems.addEventListener('blur', validarCampo);
     trm.addEventListener('blur', validarCampo);

     // Boton de enviar en el submit
     formularioEnviar.addEventListener('submit', alert);

     // Boton de reset
     resetBtn.addEventListener('click', resetFormulario);
}



// funciones
function inicioApp() {
     // deshabilitar el envio
     btnEnviar.disabled = true;
}
// Valida que el campo tengo algo escrito

function validarCampo() {
    
     // Se valida la longitud del texto y que no este vacio
     validarLongitud(this);

     let errores = document.querySelectorAll('.error');
     
     if(valor.value !== '' && listItems.value !== '' && trm.value !== '' ) {
          if(errores.length === 0) {
               btnEnviar.disabled = false;
          }
     }
     
}

// Resetear el formulario 
function resetFormulario(e) {
     formularioEnviar.reset();
     e.preventDefault();
}

// Cuando se envia el correo
function alert(e) {
     // Spinner al presionar Enviar
     const spinnerGif = document.querySelector('#spinner');
     spinnerGif.style.display = 'block';

     // Gif que envia email
     const enviado = document.createElement('img');
     enviado.src = 'img/mail.gif';
     enviado.style.display = 'block';

     // Ocultar Spinner y mostrar gif de enviado

     setTimeout(function() {
          spinnerGif.style.display = 'none';

          document.querySelector('#loaders').appendChild( enviado );

          setTimeout(function() {
               btnEnviar.disabled = false;
               enviado.remove();
               formularioEnviar.reset();
               inicioApp()
          }, 5000);
     }, 3000);

     e.preventDefault();
}

// Verifica la longitud del texto en los campos
function validarLongitud(campo) {

     if(campo.value.length > 0 ) {
          campo.style.borderBottomColor = 'green';
          campo.classList.remove('error');
     } else {
          campo.style.borderBottomColor = 'red';
          campo.classList.add('error');
     }
}



$(function(){
     $(".validar").keydown(function(event){
         //alert(event.keyCode);
         if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
             return false;
         }
     });
 });


function format(input){
     var num = input.value.replace(/\,/g,'');
     if(!isNaN(num)){
          num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,');
          num = num.split('').reverse().join('').replace(/^[\,]/,'');
          input.value = num;
     }

     else{ alert('Solo se permiten numeros');
          input.value = input.value.replace(/[^\d\.]*/g, '');
     }
}

$(document).ready(function(){
     $("#enviar").click(function(){
          enviardatos();
     });
     
     function enviardatos() {
          //Recoger datos del formulario:
          var parametros = {
               "val" : valor.value,
               "list" : listItems.value, 
               "tr" : trm.value,
          }
          $.ajax({
               type: "POST",
               url: "https://httpbin.org/post",
               data: parametros,
               before:function(){
                    console.log("se esta ejecutando");
               }
          })
          .done(function(data){
               var jsonStr = JSON.stringify(data);
               document.getElementById("resultado").innerHTML=jsonStr;
               var datos = data.form;
               $("#table-data").html(
                    "<thead>" +
                        "<tr>" +      
                            "<th> Valor </th>" +
                            "<th> Descripcion del campo seleccionado </th>" +
                            "<th> TRM </th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "</tbody>"
               );
               $("#table-data").append(
                                "<tr>" +
                                    "<td>" + datos.val + "</td>" +
                                    "<td>" + datos.list + "</td>" +
                                    "<td>" + datos.tr + "</td>" +
                                 "</tr>"
               );
            
          });
          
     }
   
     
 });

 